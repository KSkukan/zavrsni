<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Movie::class, function(Faker\Generator $faker) {
  return [
    'name' => $faker->sentence(3),
    'description' => $faker->text,
    'genre' => $faker->word,
    'director' => $faker->name,
    'duration' => $faker->numberBetween(60,180),
    'price' => $faker->numberBetween(20, 40)
  ];
});
