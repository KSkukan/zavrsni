<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToSeatUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('seat_user', function (Blueprint $table) {
            $table->integer('movie_room_id')->unsigned();
            $table->foreign('movie_room_id')
                  ->references('id')->on('movie_room')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('seat_user', function (Blueprint $table) {
            //
        });
    }
}
