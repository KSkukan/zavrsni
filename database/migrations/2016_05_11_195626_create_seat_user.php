<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeatUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seat_user', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->timestamp("reserved_at");
            $table->integer("duration")->unsigned();
            $table->integer("user_id")->unsigned();
            $table->integer("seat_id")->unsigned();
            $table->foreign("seat_id")
                  ->references("id")->on("seats")
                  ->onDelete("cascade");
             $table->foreign("user_id")
                  ->references("id")->on("users")
                  ->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('seat_user');
    }
}
