<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovieRoom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movie_room', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer("movie_id")->unsigned();
            $table->integer("room_id")->unsigned();
            $table->foreign("movie_id")
                  ->references("id")->on("movies")
                  ->onDelete("cascade");
             $table->foreign("room_id")
                  ->references("id")->on("rooms")
                  ->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('movie_room');
    }
}
