<?php

use Illuminate\Database\Seeder;
use App\Room;


class SeatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	   $rooms=DB::table('rooms')->get(['id']);
           foreach($rooms as $room) {
           	for ($red=ord('A');$red<=ord('J');$red++) {
           		for ($seat_no=1;$seat_no<=10;$seat_no++){

			          DB::table('seats')->insert([
			            'row' => chr($red),
			            'seat_num'=>$seat_no,
			            'room_id'=>$room->id,
			        ]);
			    }
           	}
           	
           }

    }
}
