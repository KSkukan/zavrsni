<?php

use Illuminate\Database\Seeder;
use App\Movie;
use App\Room;
use Carbon\Carbon;

const WEDNESDAY = 3;
const CLEANUP_TIME = 30;
const RECESS_TIME = 120;


class MovieRoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	$newDay = new Carbon();
        $movies = DB::table('movies')->get();
        $rooms = DB::table('rooms')->get(['id']);
        $today = Carbon::today();
        $nmbrOfReservations = 0;
        $counter = [];

        for ($i = 0;$i <= 60;$i++) {
        	$newDay = $today->copy(); //!!!!!!!! sta ako vec prede  u novi dan
        	$newDay->addDays($i);

        	$this->command->info('Day ' . $i);

        	foreach($movies as $movie){ // counter keeps track of how many times the movie is displayed in a day
	        		$counter[$movie->id] = 0;
	        	}
        	
        	foreach($rooms as $room){
        		
        		$this->command->info('Room ' . $room->id);
        		$time = new Carbon();
        		$time = $newDay->copy();
        		$time->hour = 11;
        		$time->minute = 0;
        		$time->second = 0;
        		while($time->hour <= 23 && $time->hour >= 11) {
        			$movie = $movies[array_rand($movies,1)];
        			$movieId = $movie->id;

        			$foundOne = false;

        			foreach($counter as $k => $cnt) {
        				if ($cnt < 2) {
        					$foundOne = true;
        					break;
        				}
        			}

        			if (!$foundOne) {
        				$this->command->info('All movies have their slots filled');
        				break;
        			}

        			if ($counter[$movieId]<2) {
        				$duration=$movie->duration;
        				$price = $movie->price;
        				
        				if ($time->dayOfWeek == WEDNESDAY) {
        					$price_modifier = 0.5;
        				} else {
        					$price_modifier = 1;
        				}

        				if ($nmbrOfReservations % 4 !== 3) {
	        				DB::table('movie_room')->insert([
					            'room_id' => $room->id,
					            'movie_id'=>$movieId,
					            'movie_start'=>$time,
					            'price_modifier'=>$price_modifier,
				        	]); 
				        	
				        	$time->addMinutes($duration);
				        	$time->addMinutes(CLEANUP_TIME);

				        	$counter[$movieId]++;
	        			} else {
	        				$time->addMinutes(RECESS_TIME);
	        			}


				        $nmbrOfReservations++;

				        $this->command->info('Moving onto ' . $time->format('H:i'));			
        			}

        		}
        	}

        }
    }
}
