@extends('layouts.master')

@section('content')
	<h1 id ="title">Movies</h1>
	<hr/>

	<ul>
		@foreach($movies as $movie)			
				<div class="container">
  					<div class="well">
						<a href="movie/{{$movie->id}}">{{$movie->name}}</a> 
						<!-- {!! Form::open(['url' => "movie/$movie->id", 'method' => 'DELETE']) !!}
						{!! Form::submit('Delete movie!', ['class'=>'btn btn-lg btn-danger']); !!}
						{!! Form::close() !!} -->
					</div>
				</div>
		@endforeach
	</ul>
	
<!-- 	<a href="/movie/create" class="btn btn-primary">
		Add Movie
	</a> -->
@endsection