@extends('layouts.master')

@section('content')

<?php use Carbon\Carbon; ?>

<h1 class="js-movie-title" data-id="{{$movie->id}}">{{$movie->name}}</h1>

<?php

$pic = strval($movie->id);

?>
<div class = "row">
  <div class="col-sm-3">
    <img src="{{ asset('img/'. $pic . '.jpg') }}"  />
  </div>  
  <div class="col-sm-9">
      <div class = "container">
        <div class = "well" style="width: 900px" >
          <p style="word-wrap:break-word"> Description: {{$movie->description}} </p>
          <p> Director: {{$movie->director}} </p>
          <p> Duration: {{$movie->duration}} minutes </p>
          <p> Genre: {{$genres[$movie->genre]}} </p>

        </div>
      </div> 
  </div>                
</div>
    <br>


<h2 class = "clear">Reserve tickets:</h2>

<select class="js-date-select">
  <?php $previous = null; ?>
  @foreach($days as $day)
     <?php
       $temp = explode(" ", $day);
       if ( $temp[0] == $previous ) { continue; }  ?>

     <option value="{{$day}}">{{date( "Y-d-m-D", strtotime($day))}}</option>

      <?php  $previous = $temp[0]; ?>

  @endforeach
</select>

<table class="times-table">
  <thead>
    <th>Room</th>
    <th>From</th>
    <th>To</th>
    <th>Price</th>
  </thead>
  <tbody class="js-times-table">
  </tbody>
</table>

@endsection