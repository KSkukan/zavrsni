@extends('layouts.master')

<?php 
    use Carbon\Carbon; 
?>
@section('content')


<h1> Reserve your seat for {{$movie->name}} </h1>
<h4> Time: {{Carbon::parse($reservation->movie_start)->format('d.m.y h:i')}} </h4>
<h4> Room: {{$room->number}}
<br>

<table class="seats" data-price="{{$price}}">
   
    @foreach($seats as $letter => $row)
    <tr>
        <td>{{$letter}}:</td>
        @foreach($row as $seat)
          <td class="seat seat-{{$letter}}-{{$seat->seat_num}} {{$seat->taken ? 'is-taken is-protected' : ''}}" data-seat="{{$seat->id}}" data-reservation="{{$reservation->id}}" data-user="{{$user->id}}" data-row="{{$letter}}" data-col="{{$seat->seat_num}}">
            <i class="material-icons icon">event_seat</i>
          </td>
        @endforeach
    </tr>  
    @endforeach
    <tr>
        <td></td>
        @foreach($seats['A'] as $seat)
            <td>{{$seat->seat_num}}</td>
        @endforeach
    </tr>
</table>

<br>
<div class="info">
    <span class="info-quantity">0</span> @ <span class="info-price">0</span> HRK
</div>
<div>

<label> Purchase seats right away <input type="checkbox" class="js-purchase" id="test"></label>
</div>

<button class="btn btn-primary js-reserve">
Reserve Seats
</button>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Credit card validation</h4>
      </div>
     
     <div class="modal-body" style="padding:40px 50px;">
      <form role="form">
        <div class="form-group">
          <input type="text" class="form-control" id="credit-card" placeholder="Enter credit card number">
        </div>
    
          <button type="button" class="btn btn-success btn-block js-validate" id = "validate"><span class="glyphicon glyphicon-ok"></span> Validate</button>

      </form>

      
        <br>
        <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
       
    </div>
      
      
    </div>
  </div>
</div>


@endsection