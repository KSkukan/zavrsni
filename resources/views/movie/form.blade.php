@extends('layouts.master')

@section('content')
  {!! Form::open(['url' => '/movie', 'method' => 'POST']) !!}
  	<div class="form-group">
	  	{!! Form::label('name', 'Movie name'); !!}
	  	{!! Form::text('name'); !!}
  	</div>
  	<div class="form-group">
	  	{!! Form::label('genre', 'Movie genre'); !!}
	  	{!! Form::select('genre', $genres); !!}
  	</div>
  	<div class="form-group">
	  	{!! Form::label('director', 'Director'); !!}
	  	{!! Form::text('director'); !!}
  	</div>
  	<div class="form-group">
	  	{!! Form::label('duration', 'Duration (minutes)'); !!}
	  	{!! Form::number('duration'); !!}
  	</div>

  	<div class="form-group">
  		{!! Form::submit('Click Me!', ['class'=>'btn btn-lg btn-primary']); !!}
	</div>


  {!! Form::close() !!}
@endsection