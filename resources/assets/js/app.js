var $ = require('jquery');
window.jQuery = $;
var bootstrap = require('bootstrap/dist/js/bootstrap');

$(function() {
	//Find elements by class
	var $dropdown = $('.js-date-select');
	var $tableBody = $('.js-times-table');
	var movieId = $('.js-movie-title').data('id');
	var $seatTable = $('.seats');
	var $reserveBtn = $('.js-reserve');
	var $validationBtn = $('.js-validate');
	var $cancelBtn = $('.pull-left');

	var basePrice = $seatTable.length ? parseInt($seatTable.data('price'), 10) : 0;

	var seatsReserved = {};

	var $quantity = $('.info-quantity');
	var $price = $('.info-price');

	function updateInfo() {
		var quantity = Object.keys(seatsReserved).length;

		$quantity.html(quantity);
		$price.html(basePrice * quantity);
	}

	// Pads a string to a certain length, if shorter than it
	function leftPad(str, len, padding) {
		var copy = String(str);

		while (copy.length < len) {
			copy = padding + copy;
		}

		return copy;
	}

	function doCancel() {
		document.getElementById("test").checked = false;
	}

	function doValidate(){

		var cardNumber = $('#credit-card').val();

		var flag = 0;
		var checksum  = 0;
    	var j = 1;
    	var i,calc;
	    for (i = cardNumber.length - 1; i >= 0; i--) {
	        calc = cardNumber.substr(i,1)*j;
	        if (calc > 9) {
	            checksum = checksum + 1;
	            calc = calc - 10;
	        }
	        checksum += calc;
	        j = (j == 1) ? 2 : 1;
	    }
	    if (checksum % 10 != 0) {
	        flag = 0;
	    }
	    else flag = 1;

	    if (flag == 0) window.alert("Invalid card number!");
	    else
	    {
	    	window.alert("Valid card number.")
	    	$("#myModal").modal("hide");
		}

	}

	function doReserve() {
		var purchase = $('.js-purchase').val();
		var requests = [];
		var userId;
		var reservationId;

		Object.keys(seatsReserved).forEach(function(key) {
			var obj = seatsReserved[key];

			if (!userId) {
				userId = obj.user;
			}

			if (!reservationId) {
				reservationId = obj.reservation;
			}

			requests.push(obj.seat);
		});

		if (requests.length === 0) {
			return;
		}


		var url = '/reserve';
		$.ajax(url, {
			method: 'POST',
			data: {
				seats: requests,
				userId: userId,
				reservationId: reservationId,
				purchase: purchase
			},
			success: function(seats) {
				reloadSeats(seats);
				seatsReserved = {};
				updateInfo();
			},
			error: function(e) {
				console.error(e);
			}
		})
	}

	function reloadSeats(seats) {
		//Replace table elements or just set the classes as appropriate
		seats.forEach(function(seat) {
			var selector = '.seat-' + seat.row + '-' + seat.seat_num;
			var $seat = $(selector);

			if (seat.taken) {
				$seat.addClass('is-taken is-protected');
			} else {
				$seat.removeClass('is-taken is-protected');
			}
		});
	}

	function reserveSeat(e) {
		var $target = $(e.currentTarget);
		var reservation = $target.data('reservation');
		var seat = $target.data('seat');
		var user = $target.data('user');
		var selector = '.seat-' + $target.data('row') + '-' + $target.data('col');

		var $seat = $(selector);

		if (!$seat.hasClass('is-taken')) {
			if (Object.keys(seatsReserved).length >= 6) {
				alert('You cannot reserve more than 6 seats at once!');
				return;
			}
		}

		$seat.toggleClass('is-taken');

		if ($seat.hasClass('is-taken')) {
			seatsReserved[selector] = {
				seat: seat,
				user: user,
				reservation: reservation
			};
		} else {
			delete seatsReserved[selector];
		}

		updateInfo();
	}

	function formatTime(date) {
		return leftPad(date.getHours(), 2, '0') + ':' + leftPad(date.getMinutes(), 2, 0);
	}

	function fillTable(times, movie) {
		times.forEach(function(time) {
			var $row = $('<tr></tr>');

			$row.on('click', function() {
				window.location.replace('/seats/' + time.id);
			});

			var startTime = new Date(time.movie_start); // Create start Date object from timestamp
			var endTime = new Date(startTime.getTime() + 1000 * 60 * parseInt(movie.duration)); // Add duration (in ms) to start to get end
			var price = parseFloat(movie.price) * parseFloat(time.price_modifier);

			var $roomNum = $('<td></td>');
			$roomNum.text(time.number);
			$row.append($roomNum);

			var $start = $('<td></td>');
			$start.text(formatTime(startTime));
			$row.append($start);

			var $end = $('<td></td>');
			$end.text(formatTime(endTime));
			$row.append($end);

			var $price = $('<td></td>');
			$price.text(String(price) + 'HRK');
			$row.append($price);

			$tableBody.append($row);
		});
	}

	//Does an API call to fetch the play times
	function fetchMoviePlayTimes(dateTime) {
		if (!dateTime) {
			return;
		}

		var date = dateTime.split(' ')[0];
		var url = '/movie/' + movieId + '/' + date

		$.ajax(url, {
			method: 'GET',
			success: function(data) {
				fillTable(data.times, data.movie);
			},
			error: function(e) {
				console.error(e);
			}
		})
	}

	// Listens to value change on event
	$dropdown.on('change', function() {
		var $this = $(this); // Element that had the event happen to it

		$tableBody.empty(); // Remove HTML from table to add new one

		fetchMoviePlayTimes($this.val());
	});

	$seatTable.on('click', '.seat:not(.is-protected)', reserveSeat);
	$reserveBtn.on('click', doReserve);
	$validationBtn.on('click', doValidate);
	$cancelBtn.on('click', doCancel);

	// Call it to populate initial table
	fetchMoviePlayTimes($dropdown.val());
	$('#test').on("change", function(e) {
		if (e.target.checked) {
	    $('#myModal').modal();
  	}
 	});

	// $('input[type="checkbox"]').on('change', function(e){
	//    if(e.target.checked){
	//      $('#myModal').modal();
	//    }
	// });
});
