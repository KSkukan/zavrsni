<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
	protected $hidden = ['image'];

    public function rooms() {
    	return $this->belongsToMany("App\Room")->withPivot(['movie_start', 'price_modifier']);
    }
}
