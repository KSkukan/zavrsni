<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Movie;
use App\Room;
use App\Seat;
use Auth;
use DB;
use Carbon\Carbon;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('movie.index', ['movies' => Movie::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function movie($id)
    {
        $today = Carbon::now()->toDateString();
        $weekFromToday = Carbon::now()->addDays(7)->toDateString();
        $movie = Movie::find($id);
     
        $days = DB::table('movie_room')->where('movie_id',$id)
                                       ->where('movie_start','>=',$today)
                                       ->where('movie_start','<=',$weekFromToday)
                                       ->pluck('movie_start');

        $genres = [
            'A' => 'Action',
            'AN' => 'Animation',
            'C' => 'Comedy',
            'CR' => 'Crime',
            'D' => 'Documentary',
            'DR' => 'Drama',
            'F' => 'Family',
            'H' => 'Horror',
            'M' => 'Musical',
            'R' => 'Romance',
            'S' => 'Sport',
            'T' => 'Thriller',
            'W' => 'Western'
        ];


        return view('movie.description',array('movie'=>$movie,'days'=> $days, 'genres'=>$genres));

    }

    public function moviesOnDate($id,$date)

    {
        $movie = Movie::with('rooms')->findOrFail($id);
        $query = "SELECT movie_room.*, rooms.number FROM movie_room JOIN rooms ON movie_room.room_id = rooms.id WHERE DATE_FORMAT(movie_start, '%Y-%m-%d') = '$date' AND movie_id" ." = '$id'";

        return response()->json(['times' => DB::select(DB::raw($query)), 'movie'=>$movie]);
    }




    public function create()
    {
        $genres = [
            'A' => 'Action',
            'AN' => 'Animation',
            'C' => 'Comedy',
            'CR' => 'Crime',
            'D' => 'Documentary',
            'DR' => 'Drama',
            'F' => 'Family',
            'H' => 'Horror',
            'M' => 'Musical',
            'R' => 'Romance',
            'S' => 'Sport',
            'T' => 'Thriller',
            'W' => 'Western'
        ];

        return view('movie.form', ['genres' => $genres]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $movie = new Movie;
        $movie->name = $request->name;
        $movie->genre = $request->genre;
        $movie->director = $request->director;
        $movie->duration = $request->duration;

        $movie->save();

        return redirect()->action('MovieController@index');
    }


    public function getSeats($timeID)
    {
        $reservation = DB::table('movie_room')->where('id',$timeID)->first();
        $movieID = $reservation->movie_id;
        $roomID = $reservation->room_id;
        $room = Room::find($roomID);
        $movie = Movie::find($movieID);
        $seats = Seat::where('room_id',$roomID)
                       ->orderBy('row', 'desc','seat_num', 'asc')->get();
        $reservedSeats = DB::table('seat_user')
                             ->where('movie_room_id', $reservation->id)->pluck('seat_id');
        $price = $reservation->price_modifier*$movie->price;

        $roomGroup = [];
        $rowGroup = null;
        $lastRow = null;
        $memoRow;

        foreach($seats as $seat) {
            if ($seat->row !== $lastRow) {
                if (!is_null($rowGroup)) {
                    $roomGroup[$lastRow] = $rowGroup;
                }

                $lastRow = $seat->row;

                $rowGroup = [];
            }

            $memoRow = $seat->row;

            if (in_array($seat->id, $reservedSeats)) {
                $seat->taken = true;
            } else {
                $seat->taken = false;
            }

            $rowGroup[] = $seat;
        }

        if (!is_null($rowGroup) && count($rowGroup > 0)) {
            $roomGroup[$memoRow] = $rowGroup;
        }
        return view('movie.seats',['movie'=>$movie,'room'=>$room, 'reservation'=>$reservation,'seats'=>$roomGroup, 'reservedSeats'=> $reservedSeats, 'user' => Auth::user(), 'price' => $price]);
    }

  
    public function reserveSeat(Request $request) {

        foreach($request->seats as $seat){
            DB::table('seat_user')->insert(
                [
                    'user_id' => $request->userId,
                    'seat_id' => $seat,
                    'movie_room_id' => $request->reservationId,
                    'bought' => $request->bought ? 1 : 0
                ]
            );
        }

        $roomId = DB::table('movie_room')->where('id', $request->reservationId)->first()->room_id;

        $room = Room::find($roomId);
        $reservedSeats = DB::table('seat_user')->where('movie_room_id', $request->reservationId)->pluck('seat_id');

        $seatList = [];

        foreach ($room->seats as $seat) {
            if (in_array($seat->id, $reservedSeats)) {
                $seat->taken = true;
            } else {
                $seat->taken = false;
            }

            $seatList[] = $seat;
        }

        return $seatList;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Movie::findOrFail($id)->delete();
        return redirect()->action('MovieController@index');
    }
}
