<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Route::resource('movie', 'MovieController');
Route::get('/movie', 'MovieController@index');
Route::get('/movie/create', 'MovieController@create');
Route::post('/movie', 'MovieController@store');
Route::delete('/movie/{id}', 'MovieController@destroy');
Route::get('movie/{id}', 'MovieController@movie');
Route::get('movie/{id}/{date}',['uses' => 'MovieController@moviesOnDate']);
Route::get('seats/{timeID}',['middleware' => 'auth','uses' => 'MovieController@getSeats']);
Route::post('/reserve','MovieController@reserveSeat');

Route::auth();

Route::get('/home', 'HomeController@index');

