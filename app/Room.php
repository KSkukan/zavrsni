<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
	public function movies() {
		return $this->belongsToMany("App\Movie");
	}

	public function seats() {
		return $this->hasMany("App\Seat");
	}

}
